//øøøøøøøøøøø
    // CUSTOMIZED DEV.
    //øøøøøøøøøøøø
    
    function initStructuredDate(parentsource){    	
    	actionOnDateSelector(parentsource);
    }
    
    function actionOnDateSelector(source){
    	//Actions dependent upon state
        
    	var state = $(findElem(source, 'dateSMKTypeSelector') + ' :selected').text();    	        
        
        switch (state)
        {
        case texts.select:
            disableDateDisplay(source);
            break;
        case texts.date:
            disableFirstDate(false, source);
            disableSecondDate(true, source);
            disableThirdDate(true, source);
            disableSegment(false, source);
            break;
        case texts.before:
            disableFirstDate(false, source);
            disableSecondDate(false, source);
            disableThirdDate(true, source);
            disableSegment(true, source);
            break;
         case texts.after:
            disableFirstDate(false, source);
            disableSecondDate(true, source);
            disableThirdDate(false, source);
            disableSegment(true, source);
            break;
         case texts.circa:
            disableFirstDate(false, source);
            disableSecondDate(false, source);
            disableThirdDate(false, source);
            disableSegment(true, source);
            break;
         case texts.decade:
            disableFirstDate(false, source);
            disableFirstDayMonth(true, source);
            disableSecondDate(true, source);
            disableThirdDate(true, source);
            break;
        case texts.centuryYear:
        case texts.century:
            disableFirstDate(false, source);
            disableFirstDayMonth(true, source);
            disableSecondDate(true, source);
            disableThirdDate(true, source);
            break;
         case texts.period:
         case texts.either:
            disableSegment(true, source);
            disableFirstDate(true, source);
            disableSecondDate(false, source);
            disableThirdDate(false, source);
            break;
         default:
            break;
        }

    }
    
    function applyChanges(applier, composeElPath, source){    	
    	    	    	
    	applier.requestChange(composeElPath("dateSMKDateDay"), $(findElem(source, 'dateSMKDateDay')).val());
   	 	applier.requestChange(composeElPath("dateSMKDateMonth"), $(findElem(source, 'dateSMKDateMonth')).val());
   	 	applier.requestChange(composeElPath("dateSMKDateYear"), $(findElem(source, 'dateSMKDateYear')).val());
   	 	applier.requestChange(composeElPath("dateSMKSecondDateDay"), $(findElem(source, 'dateSMKSecondDateDay')).val());
	 	applier.requestChange(composeElPath("dateSMKSecondDateMonth"), $(findElem(source, 'dateSMKSecondDateMonth')).val());
	 	applier.requestChange(composeElPath("dateSMKSecondDateYear"), $(findElem(source, 'dateSMKSecondDateYear')).val());
	 	applier.requestChange(composeElPath("dateSMKThirdDateDay"), $(findElem(source, 'dateSMKThirdDateDay')).val());
   	 	applier.requestChange(composeElPath("dateSMKThirdDateMonth"), $(findElem(source, 'dateSMKThirdDateMonth')).val());
   	 	applier.requestChange(composeElPath("dateSMKThirdDateYear"), $(findElem(source, 'dateSMKThirdDateYear')).val());
   	 	applier.requestChange(composeElPath("dateSMKDisplayText"), $(findElem(source, 'dateSMKDisplayText')).val());
   	 	applier.requestChange(composeElPath("dateSMKDisplayEngText"), $(findElem(source, 'dateSMKDisplayEngText')).val());
		applier.requestChange(composeElPath("dateSMKSegmentSelector"), $(findElem(source, 'dateSMKSegmentSelector')).val()); 
		applier.requestChange(composeElPath("dateSMKTypeSelector"), $(findElem(source, 'dateSMKTypeSelector')).val()); 
		applier.requestChange(composeElPath("dateSMKEraSelector"), $(findElem(source, 'dateSMKEraSelector')).val()); 
		applier.requestChange(composeElPath("dateSMKSecondEraSelector"), $(findElem(source, 'dateSMKSecondEraSelector')).val()); 
		applier.requestChange(composeElPath("dateSMKThirdEraSelector"), $(findElem(source, 'dateSMKThirdEraSelector')).val()); 
		
		var secondera = ($(findElem(source, 'dateSMKSecondEraSelector')).val()) == 0 ? 1 : -1;
		var thirdera = ($(findElem(source, 'dateSMKThirdEraSelector')).val()) == 0 ? 1 : -1;
				
		var lateDate = new Date (secondera * $(findElem(source, 'dateSMKThirdDateYear')).val(), $(findElem(source, 'dateSMKThirdDateMonth')).val() - 1, $(findElem(source, 'dateSMKThirdDateDay')).val());
		var earlyDate = new Date (thirdera * $(findElem(source, 'dateSMKSecondDateYear')).val(), $(findElem(source, 'dateSMKSecondDateMonth')).val() - 1, $(findElem(source, 'dateSMKSecondDateDay')).val());
		
		/*øøøøøøøøøø
		
		lateDate = new Date ( $(findElem(source, 'dateSMKThirdDateYear')).val(), $(findElem(source, 'dateSMKThirdDateMonth')).val() - 1, $(findElem(source, 'dateSMKThirdDateDay')).val());
		earlyDate = new Date ( $(findElem(source, 'dateSMKSecondDateYear')).val(), $(findElem(source, 'dateSMKSecondDateMonth')).val() - 1, $(findElem(source, 'dateSMKSecondDateDay')).val());		
		
		var secondera2 = ($(findElem(source, 'dateSMKSecondEraSelector')).val()) == 0 ? ' AD' : ' BC';
		var thirdera2 = ($(findElem(source, 'dateSMKThirdEraSelector')).val()) == 0 ? ' AD' : ' BC';
		
		//var lateDate2 = new Date ($(findElem(source, 'dateSMKThirdDateYear')).val(), $(findElem(source, 'dateSMKThirdDateMonth')).val() - 1, $(findElem(source, 'dateSMKThirdDateDay')).val()).toString() + secondera2;
		//var earlyDate2 = new Date ($(findElem(source, 'dateSMKSecondDateYear')).val(), $(findElem(source, 'dateSMKSecondDateMonth')).val() - 1, $(findElem(source, 'dateSMKSecondDateDay')).val()).toString() + earlyDate2;

		//var lateDatestamp = lateDate.getTime() / 1000;
		//var earlyDatestamp = earlyDate.getTime() / 1000;
		
		var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

		var lateDate_day = lateDate.getDate();
		var lateDate_month = lateDate.getMonth();
		var lateDate_year = lateDate.getFullYear();
		var lateDate_ADBC = ($(findElem(source, 'dateSMKSecondEraSelector')).val()) == 0 ? 'AD' : 'BC';
		var earlyDate_day = earlyDate.getDate();
		var earlyDate_month = earlyDate.getMonth();
		var earlyDate_year = earlyDate.getFullYear();
		var earlyDate_ADBC = ($(findElem(source, 'dateSMKThirdEraSelector')).val()) == 0 ? 'AD' : 'BC';
		
		var lateDatestamp = m_names[lateDate_month] + " " + lateDate_day + ", " + lateDate_year + " " + lateDate_ADBC;
		var earlyDatestamp = m_names[earlyDate_month] + " " + earlyDate_day + ", " + earlyDate_year + " " + earlyDate_ADBC;
		//applier.requestChange(composeElPath("dateSMKEarliestScalarValue"), 'January 8, 99 AD');
		
		/*øøøøøøøøøø*/
		
		applier.requestChange(composeElPath("dateSMKLatestScalarValue"), lateDate);
		applier.requestChange(composeElPath("dateSMKEarliestScalarValue"), earlyDate);
		
    	    			
    }
    
    function actionOnFirstDate(source){    	
    	var input = new userData(source);        
        if((input.year != ''  && input.day == '' ) ||
           (input.year != '' && input.month != '')){
            updateDateUI(input);
            updateDisplayText(input);
            generateEarliestLatest(input);
        }
        else{
            clearCSdates(source);
        }              	
    }
    
    function actionOnSecondDate(source){    	
    	var input = new userData(source);
        if(input.secondYear != ''){
            updateDisplayText(input);
        }
    }
    
    function actionOnThirdDate(source){    	
    	var input = new userData(source);
        if(input.thirdYear != ''){
            updateDisplayText(input);
        }
    }
    
    function getElem(source){    	    	
    	var elem = "#" + source;    	    	
    	return elem;    	
    }
    
    function findElem(parentsource, id){    	    	
    	var elem = parentsource.find('[id^='+ id +']');
    	return getElem(elem.attr("id"));    	
    }        
    
    cspace.structuredDateSMK.popup.updateScalarValues = function (model, changeRequest, popup) {
        if (!popup) {
            return;
        }
        var applier = popup.applier,
            composeElPath = popup.composeElPath,
            refreshView = popup.refreshView,
            defaultFormat = popup.options.defaultFormat,
            displayScalars = popup.options.displayScalars;
        
        if(changeRequest[0].source){
        	
        	var parentsource = $(getElem(changeRequest[0].source)).closest('div.csc-structuredDateSMK-popup-container');
        	var input = new userData(parentsource, findElem);
        	
        	switch(changeRequest[0].path)
        	
        	{        		
        		case composeElPath("dateSMKDateDay"): 
        			var elem = getElem(changeRequest[0].source);       			
        			$(elem).val(validateDay($(elem).val()));
        			actionOnFirstDate(input.source); 
                  	break;
                
        		case composeElPath("dateSMKDateMonth"):             	      	        		             	
        			var elem = getElem(changeRequest[0].source);
        			$(elem).val(validateMonth($(elem).val()));
                   	actionOnFirstDate(input.source); 
                   	break;
                
        		case composeElPath("dateSMKDateYear"):
        			var elem = getElem(changeRequest[0].source);
        			$(elem).val(validateYear($(elem).val(), input.state));
              		actionOnFirstDate(input.source);
              		break;  
              	
        		case composeElPath("dateSMKSecondDateDay"):         			
        			var elem = getElem(changeRequest[0].source);
    				$(elem).val(validateDay($(elem).val()));
        			actionOnSecondDate(input.source); 
                  	break;
                
        		case composeElPath("dateSMKSecondDateMonth"):             	      	        		             	
        			var elem = getElem(changeRequest[0].source);
    				$(elem).val(validateMonth($(elem).val()));
    				actionOnSecondDate(input.source);  
                   	break;
                
        		case composeElPath("dateSMKSecondDateYear"):
        			var elem = getElem(changeRequest[0].source);
        			$(elem).val(validateYear($(elem).val(), input.state));
    				actionOnSecondDate(input.source); 
              		break;	
              	
        		case composeElPath("dateSMKThirdDateDay"):         			
        			var elem = getElem(changeRequest[0].source);
    				$(elem).val(validateDay($(elem).val()));
        			actionOnThirdDate(input.source); 
                  	break;
                
        		case composeElPath("dateSMKThirdDateMonth"):             	      	        		             	
        			var elem = getElem(changeRequest[0].source);
    				$(elem).val(validateMonth($(elem).val()));
    				actionOnThirdDate(input.source);  
                   	break;
                
        		case composeElPath("dateSMKThirdDateYear"):
        			var elem = getElem(changeRequest[0].source);
        			$(elem).val(validateYear($(elem).val(), input.state));
    				actionOnThirdDate(input.source);
              		break;	              	        		
              		
        		case composeElPath("dateSMKEraSelector"):         	         	        
        	        if((input.year != ''  && input.day == '' ) ||
        	           (input.year != '' && input.month != '')){
        	            updateDisplayText(input);
        	            generateEarliestLatest(input);
        	        }
        	        else{
        	            clearCSdates(input.source);
        	        }
        		
        	        break;
        		
        		case composeElPath("dateSMKSecondEraSelector"):         	            	        
        	        updateDisplayText(input); 
        	        break;
        		
        		case composeElPath("dateSMKThirdEraSelector"):         	    
        	        updateDisplayText(input); 
        	        break;
        		
        		case composeElPath("dateSMKSegmentSelector"):         	        
        	        updateDisplayText(input); 
        	        generateEarliestLatest(input);
        	        break;
        		
        		case composeElPath("dateSMKTypeSelector"): 
        	        clearInputFields(input.source);
        	        actionOnDateSelector(input.source);       
        	        break;
                
        	}    
        	
        	applyChanges(applier, composeElPath, input.source);
        	                   	
         }         	         	         	         	    
   };   
    
    