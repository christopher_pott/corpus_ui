var texts = {
		    "select" : 'Vælg',
		    "date" : 'Dato',
		    "before" : 'Før',
		    "after" : 'Efter',
		    "circa" : 'Circa',
		    "decade" : 'Årti',
		    "century" : 'Århundrede',
		    "period" : 'Periode',
		    "centuryYear" : '-tallet',
		    "either" : 'Enten/Eller',
		    "whole" : 'Hele',
		    "firstHalf" : '1. halvdel af',
		    "secondHalf" : '2. halvdel af',
		    "firstQuarter" : '1. fjerdedel af',
		    "secondQuarter" : '2. fjerdedel af',
		    "thirdQuarter" : '3. fjerdedel af',
		    "forthQuarter" : '4. fjerdedel af',
		    "early" : 'Begyndelsen af',
		    "mid" : 'Midten af',
		    "late" : 'Slutningen af',
		    "dateType" : 'Dato Type',
		    "year" : 'År',
		    "month" : 'Måned',
		    "day" : 'Dag',
		    "era" : 'Æra',
		    "segment" : 'Segment',
		    "earliestDate" : 'Tidligste dato',
		    "latestDate" : 'Seneste dato',
		    "displayText" : 'Displaytekst',
		    "displayTextEn" : 'Engelsk displaytekst',
		    "save" : 'Save',
		    "ad" : 'A.D.',
		    "bc" : 'f.Kr.'
		}
	
	var eras = {
		    "0" : texts.ad,
		    "1" : texts.bc
		};

	var datetypes = {
	    "0" : texts.select,
	    "1" : texts.date,
	    "2" : texts.before,
	    "3" : texts.after,
	    "4" : texts.circa,
	    "5" : texts.decade,
	    "6" : texts.century,
	    "7" : texts.period,
	    "8" : texts.centuryYear,
	    "9" : texts.either
	};

	var segments = {
	    "0" : texts.whole,
	    "1" : texts.firstHalf,
	    "2" : texts.secondHalf,
	    "3" : texts.firstQuarter,
	    "4" : texts.secondQuarter,
	    "5" : texts.thirdQuarter,
	    "6" : texts.forthQuarter,
	    "7" : texts.early,
	    "8" : texts.mid,
	    "9" : texts.late
	};

	/* These three define the time scales for the segments
	 * The order MUST be the same as the 'segments' listed above
	 * */
	var yearSegment = {
	        "months" : [
	        /* "Whole":*/          {earliest:0,latest:12},
	        /* "firstHalf":*/      {earliest:0,latest:6},
	        /*  "secondHalf":*/    {earliest:6,latest:12},
	        /*  "firstQuarter":*/  {earliest:0,latest:3},
	        /*  "secondQuarter":*/ {earliest:3,latest:6},
	        /*  "thirdQuarter":*/  {earliest:6,latest:9},
	        /*  "forthQuarter":*/  {earliest:9,latest:12},
	        /*  "early":*/         {earliest:0,latest:4},
	        /*  "mid":*/           {earliest:4,latest:8},
	        /*  "late":*/          {earliest:8,latest:12}
	        ]
	    };

    var decadeSegment = {
        "months" : [
        /* "Whole":*/          {earliest:0,latest:120},
        /* "firstHalf":*/      {earliest:0,latest:60},
        /*  "secondHalf":*/    {earliest:60,latest:120},
        /*  "firstQuarter":*/  {earliest:0,latest:30},
        /*  "secondQuarter":*/ {earliest:30,latest:60},
        /*  "thirdQuarter":*/  {earliest:60,latest:90},
        /*  "forthQuarter":*/  {earliest:90,latest:120},
        /*  "early":*/         {earliest:0,latest:48},
        /*  "mid":*/           {earliest:48,latest:90},
        /*  "late":*/          {earliest:90,latest:120}
        ]
    };

    /*years to add based upon segment type*/
    var centurySegment = {
        "years" : [
        /* "Whole":*/          {earliest:1,latest:101},
        /* "firstHalf":*/      {earliest:1,latest:51},
        /*  "secondHalf":*/    {earliest:51,latest:101},
        /*  "firstQuarter":*/  {earliest:1,latest:26},
        /*  "secondQuarter":*/ {earliest:26,latest:51},
        /*  "thirdQuarter":*/  {earliest:51,latest:76},
        /*  "forthQuarter":*/  {earliest:76,latest:101},
        /*  "early":*/         {earliest:1,latest:34},
        /*  "mid":*/           {earliest:34,latest:67},
        /*  "late":*/          {earliest:67,latest:101}
        ]
    }; 