//øøø smkdate.js
   
   function validateDay(day) {

       var value = day.replace(/[^0-9\.]/g,'');
       var maxDayValue = 31;

       if (value > maxDayValue) {
           value = value.slice(0, 1);
       }
       if (value == 0) {
           value = '';
       }
       return value;
   }

   function validateMonth(month) {

       var value = month.replace(/[^0-9\.]/g,'');
       var maxMonthValue = 12;

       if (value > maxMonthValue) {
           value = value.slice(0, 1);
       }
       if (value == 0) {
           value = '';
       }
       return value;
   }

   function validateYear(year, state) {
   	    	
       var value = year.replace(/[^0-9\.]/g,'');
       var maxYearValue = 9999;
       /*max length is shorter for 'century' so we cannot rely
         on the html max-length property*/
       var maxLength = 4;

       //if($('#dateSMKTypeSelector :selected').text() == texts.century){
       if(state == texts.century){
           maxLength = 2;
       }

       if (value > maxYearValue) {
           value = value.slice(0, 4);
       }
       if (value == 0) {
           value = '';
       }
       else if (value.length >= maxLength){
           value = value.slice(0, maxLength);
       }
       return value;
   }          

   /*months to add based upon segment type*/

   function dayIsValid(date, originalDay){

       if (originalDay != '' && date.getDate() != originalDay){
           return false;
       }
       return true;
   }

   function validateEarliestLatest(input) {

       var error = '';
       var earliestDate = new Date();
       var latestDate = new Date();

       if (input.secondYear == '' || input.thirdYear == ''){
           alert(dkFormatMissingYear(input));
           return;
       }
       else {
           earliestDate.setFullYear(input.secondYear);
           earliestDate.setMonth((input.secondMonth == '') ?  0 : input.secondMonth - 1);
           earliestDate.setDate((input.secondDay == '') ? 1 : input.secondDay);
           if (input.secondEra == texts.bc){
               earliestDate.setFullYear(input.secondYear * -1);
           }
           if (!dayIsValid(earliestDate, input.secondDay)){
                alert(dkFormatWrongSecondDays(input));
                //øø $('#dateSMKSecondDateDay').val('');
                $(findElem(input.source, 'dateSMKSecondDateDay')).val('');
                return;
           }
           latestDate.setFullYear(input.thirdYear);
           latestDate.setMonth((input.thirdMonth == '') ?  11 : input.thirdMonth - 1);
           (input.thirdDay == '') ? latestDate.moveToLastDayOfMonth() : latestDate.setDate(input.thirdDay);
           if (input.thirdEra == texts.bc){
               latestDate.setFullYear(input.thirdYear * -1);
           }
           if (!dayIsValid(latestDate, input.thirdDay)){
                alert(dkFormatWrongThirdDays(input));
                //øø $('#dateSMKThirdDateDay').val('');
                $(findElem(input.source, 'dateSMKThirdDateDay')).val('');
                return;
           }
           if (earliestDate > latestDate){
               alert(dkFormatCompareError(input));
           }
           else{
               updateEarliestDisplay(earliestDate.getDate(), earliestDate.getMonth() + 1, earliestDate.getFullYear());
               updateLatestDisplay(latestDate.getDate(), latestDate.getMonth() + 1, latestDate.getFullYear());
           }
       }
   }

   function generateEarliestLatest(input) {
       
       /*Convert the input fields to Date objects so that
         we can apply date arithmetic to them and check them
         for correctness. They will need converting back to 
         fields before sending to services
         */

       /*init earliest date*/
       var earliestDate = new Date();
       earliestDate.setFullYear(input.year); /*setYear() will not set 0-99AD correctly*/
       earliestDate.setMonth((input.month == '') ? 0 : input.month - 1);
       earliestDate.setDate((input.day == '') ? 1 : input.day);

       if (input.era == texts.bc){
           earliestDate.setFullYear(input.year * -1);
       }

       /*Check if number of days is valid for the given month and year*/
       if (!dayIsValid(earliestDate, input.day)){
            alert(dkFormatWrongDays(input));
            //øø $('#dateSMKDateDay').val(input.day.slice(0, 1));
            $(findElem(input.source, 'dateSMKDateDay')).val(input.day.slice(0, 1));
            return;
       }

       /*init latest date*/
       var latestDate = new Date();
       latestDate.setTime(earliestDate.getTime());

       switch (input.state)
       {
       case texts.select:
           break;
       case texts.date:
           if (input.month == ''){ /*year*/
               earliestDate.add({months :yearSegment.months[input.segmentIndex].earliest});
               latestDate.add({months :yearSegment.months[input.segmentIndex].latest});
               latestDate.add({days :-1});
           }
           else if (input.day == ''){ /*month*/
              latestDate.add({months :1});
              latestDate.add({days :-1});
           }
           break;
       case texts.before:
           earliestDate.add({years :-100});
           break;
       case texts.after:
           latestDate.add({years :100});
           break;
       case texts.circa:
           earliestDate.add({years :-2});
           latestDate.add({years :2});
           break;
       case texts.decade:
           var startYear = earliestDate.getFullYear() - (earliestDate.getFullYear() % 10);
           earliestDate.setFullYear(startYear);
           latestDate.setFullYear(startYear);
           earliestDate.add({months :decadeSegment.months[input.segmentIndex].earliest});
           latestDate.add({months :decadeSegment.months[input.segmentIndex].latest});
           latestDate.add({days :-1});
           break;
       case texts.century:
           /*perform calulations as AD*/
           var startYear = earliestDate.getFullYear() ;
           if (input.era == texts.bc){
               startYear = startYear * -1;
           }
           startYear = (startYear - 1) * 100;
           earliestDate.setFullYear(startYear);
           latestDate.setFullYear(earliestDate.getFullYear());
           earliestDate.add({years :centurySegment.years[input.segmentIndex].earliest});
           latestDate.add({years :centurySegment.years[input.segmentIndex].latest});
           latestDate.add({days :-1});

           /*if Century is BC then just swap years and make negative*/
           if (input.era == texts.bc){
               var earliest = latestDate.getFullYear() * -1;
               latestDate.setFullYear(earliestDate.getFullYear() * -1);
               earliestDate.setFullYear(earliest);
           }
           break;
       case texts.centuryYear:
           earliestDate.add({years :centurySegment.years[input.segmentIndex].earliest});
           latestDate.add({years :centurySegment.years[input.segmentIndex].latest});
           latestDate.add({days :-1});
           break;
       case texts.either:
       case texts.period:
       default:
           break;
       }
       updateEarliestDisplay(earliestDate.getDate(), earliestDate.getMonth() + 1, earliestDate.getFullYear(), input.source);
       updateLatestDisplay(latestDate.getDate(), latestDate.getMonth() + 1, latestDate.getFullYear(), input.source);
   }

   function updateEarliestDisplay(day, month, year, source) {

       var era = '0';
       if(year < 0){
           year = year * -1;
           era = '1';
       }

       //øø $('#dateSMKSecondDateDay').val(day);
       //øø $('#dateSMKSecondDateMonth').val(month);
       //øø $('#dateSMKSecondDateYear').val(year);        
       $(findElem(source, 'dateSMKSecondDateDay')).val(day);
       $(findElem(source, 'dateSMKSecondDateMonth')).val(month);
       $(findElem(source, 'dateSMKSecondDateYear')).val(year);
       

       /*have to check if era is readonly and if it is then we need to set it 
         again for the changed value*/
       /* øøø
       if ($('#dateSMKSecondEraSelector').val() != era){
           if ($('#dateSMKSecondEraSelector option').not(':selected').attr('disabled') == true){
               $('#dateSMKSecondEraSelector').val(era).attr('selected', 'selected');
               $('#dateSMKSecondEraSelector option').not(':selected').attr('disabled', 'disabled');
           }
           else{
               $('#dateSMKSecondEraSelector').val(era).attr('selected', 'selected');
            }
       }*/
       if ($(findElem(source, 'dateSMKSecondEraSelector')).val() != era){
           if ($(findElem(source, 'dateSMKSecondEraSelector') + 'option').not(':selected').attr('disabled') == true){
           	$(findElem(source, 'dateSMKSecondEraSelector')).val(era).attr('selected', 'selected');            	            	
               $(findElem(source, 'dateSMKSecondEraSelector') + 'option').not(':selected').attr('disabled', 'disabled');
           }
           else{
               $(findElem(source, 'dateSMKSecondEraSelector')).val(era).attr('selected', 'selected');
            }
       }
       
    }

  function updateLatestDisplay(day, month, year, source) {
    
       var era = '0';
       if(year < 0){
           year = year * -1;
           era = '1'
       }

       //øø $('#dateSMKThirdDateDay').val(day);
       //øø $('#dateSMKThirdDateMonth').val(month);
       //øø $('#dateSMKThirdDateYear').val(year);	        
       $(findElem(source, 'dateSMKThirdDateDay')).val(day);
       $(findElem(source, 'dateSMKThirdDateMonth')).val(month);
       $(findElem(source, 'dateSMKThirdDateYear')).val(year);

       /*
       if ($('#dateSMKThirdEraSelector').val() != era){
           if ($('#dateSMKThirdEraSelector option').not(':selected').attr('disabled') == true){
               $('#dateSMKThirdEraSelector').val(era).attr('selected', 'selected');
               $('#dateSMKThirdEraSelector option').not(':selected').attr('disabled', 'disabled');
           }
           else{
               $('#dateSMKThirdEraSelector').val(era).attr('selected', 'selected');
           }
       }*/
       if ($(findElem(source, 'dateSMKThirdEraSelector')).val() != era){
           if ($(findElem(source, 'dateSMKThirdEraSelector') + 'option').not(':selected').attr('disabled') == true){
           	$(findElem(source, 'dateSMKThirdEraSelector')).val(era).attr('selected', 'selected');            	            	
               $(findElem(source, 'dateSMKThirdEraSelector') + 'option').not(':selected').attr('disabled', 'disabled');
           }
           else{
               $(findElem(source, 'dateSMKThirdEraSelector')).val(era).attr('selected', 'selected');
            }
       }
       
  }

   function updateDateUI(input) { 

       //Hide 'segment' if more than year is selected
       switch (input.state)
       {
       case texts.date:
       case texts.decade:
       case texts.centuryYear:
       case texts.century:
           if(input.day != ''){
               disableSegment(true, input.source);
           }
           if (input.month != ''){
               disableSegment(true, input.source);
           }
           if ((input.day == '') && (input.month == '')){
               disableSegment(false, input.source);
           }
           break;
       default:
           disableSegment(true, input.source);
           break;
       }
       
       /*re-read relevant vars and update directly, objects are passed by ref*/
       //øø input.segment = $('#dateSMKSegmentSelector :selected').text();
       //øø input.segmentIndex = $('#dateSMKSegmentSelector :selected').val();
       
       input.segment = $(findElem(input.source, 'dateSMKSegmentSelector') + " :selected").text();
       input.segmentIndex = $(findElem(input.source, 'dateSMKSegmentSelector') + " :selected").val();
   }

   function userData(source) {        
       
   	this.source = source;
   	
   	//first date
       this.day = $(findElem(source, 'dateSMKDateDay')).val();
       this.month = $(findElem(source,'dateSMKDateMonth')).val();
       this.year = $(findElem(source,'dateSMKDateYear')).val();               
       this.era = $(findElem(source,'dateSMKEraSelector') + ' :selected').text();

       //second date
       this.secondDay = $(findElem(source,'dateSMKSecondDateDay')).val();
       this.secondMonth = $(findElem(source,'dateSMKSecondDateMonth')).val();
       this.secondYear = $(findElem(source,'dateSMKSecondDateYear')).val();
       this.secondEra = $(findElem(source,'dateSMKSecondEraSelector') + ' :selected').text();

       //third date
       this.thirdDay = $(findElem(source,'dateSMKThirdDateDay')).val();
       this.thirdMonth = $(findElem(source,'dateSMKThirdDateMonth')).val();
       this.thirdYear = $(findElem(source,'dateSMKThirdDateYear')).val();
       this.thirdEra = $(findElem(source,'dateSMKThirdEraSelector') + ' :selected').text();

       //Additional fields
       this.segment = $(findElem(source,'dateSMKSegmentSelector') + ' :selected').text();
       this.segmentIndex = $(findElem(source,'dateSMKSegmentSelector') + ' :selected').val();
       this.state = $(findElem(source,'dateSMKTypeSelector') + ' :selected').text();
       
       
   	/*
       //first date
       this.day = $('#dateSMKDateDay').val();
       this.month = $('#dateSMKDateMonth').val();
       this.year = $('#dateSMKDateYear').val();
       this.era = $('#dateSMKEraSelector :selected').text();

       //second date
       this.secondDay = $('#dateSMKSecondDateDay').val();
       this.secondMonth = $('#dateSMKSecondDateMonth').val();
       this.secondYear = $('#dateSMKSecondDateYear').val();
       this.secondEra = $('#dateSMKSecondEraSelector :selected').text();

       //third date
       this.thirdDay = $('#dateSMKThirdDateDay').val();
       this.thirdMonth = $('#dateSMKThirdDateMonth').val();
       this.thirdYear = $('#dateSMKThirdDateYear').val();
       this.thirdEra = $('#dateSMKThirdEraSelector :selected').text();

       //Additional fields
       this.segment = $('#dateSMKSegmentSelector :selected').text();
       this.segmentIndex = $('#dateSMKSegmentSelector :selected').val();
       this.state = $('#dateSMKTypeSelector :selected').text();
       
       */
    }

    
   function updateDisplayText(input) {     
       //øø $('#dateSMKDisplayText').val(dkFormatDate(input));
   	$(findElem(input.source, 'dateSMKDisplayText')).val(dkFormatDate(input));
   	$(findElem(input.source, 'dateSMKDisplayEngText')).val(formatDate(input));
   	
   }

   function disableDateDisplay(source){

       disableFirstDate(true, source);
       disableSecondDate(true, source);
       disableThirdDate(true, source);
       disableSegment(true, source);
   }
   
   function disableFirstDayMonth(status, source){

       //øø $('#dateSMKDateDay, #dateSMKDateMonth').attr('readonly', status);
   	$(findElem(source, 'dateSMKDateDay')).attr('readonly', status);
   	$(findElem(source, 'dateSMKDateMonth')).attr('readonly', status);
       
       if(status == true){
           //øø $('#dateSMKDateDay, #dateSMKDateMonth').addClass('disabledField');
           $(findElem(source, 'dateSMKDateDay')).addClass('disabledField');
           $(findElem(source, 'dateSMKDateMonth')).addClass('disabledField');
       }
       else{
           //øø $('#dateSMKDateDay, #dateSMKDateMonth').removeClass('disabledField');
           $(findElem(source, 'dateSMKDateDay')).removeClass('disabledField');
           $(findElem(source, 'dateSMKDateMonth')).removeClass('disabledField');
       }
   }    

   function disableSegment(status, source){

   	//øø var fix = $('#dateSMKSegmentSelector');
   	var fix = $(findElem(source, 'dateSMKSegmentSelector'));
   	fix.attr('disabled', status);

       if (status == true){
           //øø $('#dateSMKSegmentSelector').val('0');
           //øø $('#dateSMKSegmentSelector').addClass('disabledField');            
           $(findElem(source, 'dateSMKSegmentSelector')).val('0');
           $(findElem(source, 'dateSMKSegmentSelector')).addClass('disabledField');
       }
       else {
           //øø $('#dateSMKSegmentSelector').removeClass('disabledField');
           $(findElem(source, 'dateSMKSegmentSelector')).removeClass('disabledField');
       }
   }
  
   function disableFirstDate(status, source){

       //øø $('#dateSMKDateDay, #dateSMKDateMonth, #dateSMKDateYear').attr('readonly', status);
   	$(findElem(source, 'dateSMKDateDay')).attr('readonly', status);
   	$(findElem(source, 'dateSMKDateMonth')).attr('readonly', status);
   	$(findElem(source, 'dateSMKDateYear')).attr('readonly', status);
   	
       if(status == true){
           //øø $('#dateSMKEraSelector option').not(':selected').attr('disabled', 'disabled');
           //øø $('#first-date input[type="text"], #dateSMKEraSelector').addClass('disabledField');        	                      	            	
           $(findElem(source, 'dateSMKEraSelector') + ' option').not(':selected').attr('disabled', 'disabled');
           $(findElem(source, 'dateSMKEraSelector')).addClass('disabledField');
           $(findElem(source, 'first-date') + ' input[type="text"]').addClass('disabledField');
       }
       else{
           //øø $('#dateSMKEraSelector option').not(':selected').removeAttr('disabled');
           //øø $('#first-date input[type="text"], #dateSMKEraSelector').removeClass('disabledField');
           
           $(findElem(source, 'dateSMKEraSelector') + ' option').not(':selected').removeAttr('disabled');
           $(findElem(source, 'dateSMKEraSelector')).removeClass('disabledField');
           $(findElem(source, 'first-date') + ' input[type="text"]').removeClass('disabledField');
           
       }
   }

   function disableSecondDate(status, source){
   	
   	$(findElem(source, 'dateSMKSecondDateDay')).attr('readonly', status);
   	$(findElem(source, 'dateSMKSecondDateMonth')).attr('readonly', status);
   	$(findElem(source, 'dateSMKSecondDateYear')).attr('readonly', status);
   	
       if(status == true){     	                      	            	
           $(findElem(source, 'dateSMKSecondEraSelector') + ' option').not(':selected').attr('disabled', 'disabled');
           $(findElem(source, 'dateSMKSecondEraSelector')).addClass('disabledField');
           $(findElem(source, 'second-date') + ' input[type="text"]').addClass('disabledField');
       }
       else{
           $(findElem(source, 'dateSMKSecondEraSelector') + ' option').not(':selected').removeAttr('disabled');
           $(findElem(source, 'dateSMKSecondEraSelector')).removeClass('disabledField');
           $(findElem(source, 'second-date') + ' input[type="text"]').removeClass('disabledField');            
       }
   	/*
       $('#dateSMKSecondDateDay, #dateSMKSecondDateMonth, #dateSMKSecondDateYear').attr('readonly', status);

       if(status == true){
           $('#dateSMKSecondEraSelector option').not(':selected').attr('disabled', 'disabled');
           $('#second-date input[type="text"], #dateSMKSecondEraSelector').addClass('disabledField');
       }
       else{
           $('#dateSMKSecondEraSelector option').not(':selected').removeAttr('disabled');
           $('#second-date input[type="text"], #dateSMKSecondEraSelector').removeClass('disabledField');
       }
       */
   }

   function disableThirdDate(status, source){

   	$(findElem(source, 'dateSMKThirdDateDay')).attr('readonly', status);
   	$(findElem(source, 'dateSMKThirdDateMonth')).attr('readonly', status);
   	$(findElem(source, 'dateSMKThirdDateYear')).attr('readonly', status);
   	
       if(status == true){     	                      	            	
           $(findElem(source, 'dateSMKThirdEraSelector') + ' option').not(':selected').attr('disabled', 'disabled');
           $(findElem(source, 'dateSMKThirdEraSelector')).addClass('disabledField');
           $(findElem(source, 'third-date') + ' input[type="text"]').addClass('disabledField');
       }
       else{
           $(findElem(source, 'dateSMKThirdEraSelector') + ' option').not(':selected').removeAttr('disabled');
           $(findElem(source, 'dateSMKThirdEraSelector')).removeClass('disabledField');
           $(findElem(source, 'third-date') + ' input[type="text"]').removeClass('disabledField');            
       }
   	
   	/*
       $('#dateSMKThirdDateDay, #dateSMKThirdDateMonth, #dateSMKThirdDateYear').attr('readonly', status);

       if(status == true){
           $('#dateSMKThirdEraSelector option').not(':selected').attr('disabled', 'disabled');
           $('#third-date input[type="text"], #dateSMKThirdEraSelector').addClass('disabledField');
       }
       else{
           $('#dateSMKThirdEraSelector option').not(':selected').removeAttr('disabled');
           $('#third-date input[type="text"], #dateSMKThirdEraSelector').removeClass('disabledField');
       }
       */
   }

   function enableDateDisplay(source){

       //enableDayMonth();
       //øø $('#dateSMKDateYear, #dateSMKEraSelector, #dateSMKDisplayText').attr('readonly', false);
       
       $(findElem(source, 'dateSMKDateYear')).attr('readonly', false);
   	$(findElem(source, 'dateSMKEraSelector')).attr('readonly', false);
   	$(findElem(source, 'dateSMKDisplayText')).attr('readonly', false);
   	$(findElem(source, 'dateSMKDisplayEngText')).attr('readonly', false);
   }

   function enableFirstDateDayMonth(status, source){

       //øø $('#dateSMKDateDay, #dateSMKDateMonth').attr('readonly', status);
       $(findElem(source, 'dateSMKDateDay')).attr('readonly', status);
   	$(findElem(source, 'dateSMKDateMonth')).attr('readonly', status);
       
   }
   
   function clearCSdates(source){

       //øø $('#second-date :input, #third-date :input').val('');        
       $(findElem(source, 'second-date') + ' :input').val('');
       $(findElem(source, 'third-date')+ ' :input').val('');
   }

    function clearInputFields(parentcontent){

       //øø $(':input').not('#dateSMKTypeSelector, #dateSMKSegmentSelector').val('');    	 
   	 var elemType = findElem(parentcontent, 'dateSMKTypeSelector');
   	 var elemSelect = findElem(parentcontent, 'dateSMKSegmentSelector');
   	 var elemParent = $(parentcontent).find(':input');
   	 $(elemParent).not(elemType + ', ' + elemSelect).val('');
   	     	 
   }