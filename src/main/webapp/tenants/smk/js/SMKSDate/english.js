function getQualifer(num){

    var qualifier = 'th';

    switch(num) {
    case '':
        qualifier = '';
        break;
    case '1':
    case '21':
    case '31':
        qualifier = 'st';
        break;
    case '2':
    case '22':
        qualifier = 'nd';
        break;
    case '3':
    case '23':
        qualifier = 'rd';
        break;
    default:
        break;
    }
    return ((qualifier == '') ? '' : num + qualifier + ' ');
}

function getMonth(index){
    
    var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    var month = '';
    
    if(index <= months.length && index > 0){
       month = months[index-1];
    }
    return ((month == '') ? '' : month + ' ');
    
}

function getSegment(txt){

   var seg = '';

    switch(txt){
    case segments[0]:
        seg = 'Whole';
        break;
    case segments[1]:
        seg = 'First half';
        break;
    case segments[2]:
        seg = 'Second half';
        break;
    case segments[3]:
        seg = 'First quarter';
        break;
    case segments[4]:
        seg = 'Second quarter';
        break;
    case segments[5]:
        seg = 'Third quarter';
        break;
    case segments[6]:
        seg = 'Forth quarter';
        break;
    case segments[7]:
        seg = 'Early';
        break;
    case segments[8]:
        seg = 'Mid';
        break;
    case segments[9]:
        seg = 'Late';
        break;
    }

    return ((txt == texts.whole) ? '' : seg + ' ');
}

function getEra(txt){
    
    return ((txt == texts.ad) ? '' : 'BC ');
}

function getDay(txt){
    
    return ((txt == '') ? '' : txt + ' ');
}

function getYear(txt){
    
    return ((txt == '') ? '' : txt + ' ');
}

function getSimpleDate(date, month, year, era){
    
    var txt = '';
    
    if (year != ''){
        if (month != ''){
            if (date != ''){
                txt = date + '/';
                txt = date + '/' + month + '/';
            }
            else{
                txt = getMonth(month);
            }
        }    
        txt = txt + year;
        if (era == texts.bc){
            txt = txt + ' BC';
        }
    }
    return txt;
}

function formatDate(input){
    
    var display = '';
    
    switch (input.state)
    {
    case texts.select:
        break;
    case texts.date:
        display = getSegment(input.segment) + getQualifer(input.day) + getMonth(input.month) + 
                  getYear(input.year) + getEra(input.era);
        break;
    case texts.before:
        display = 'Before ' + getQualifer(input.day) + getMonth(input.month) + 
                  getYear(input.year) + getEra(input.era);
        break;
    case texts.after:
        display = 'After ' + getQualifer(input.day) + getMonth(input.month) + 
                  getYear(input.year) + getEra(input.era);
        break;
    case texts.circa:
        display = 'Circa ' + getQualifer(input.day) + getMonth(input.month) + 
                  getYear(input.year) + getEra(input.era);
        break;
    case texts.decade:
        display = getSegment(input.segment) + input.year + '\'s '+ getEra(input.era);
        break;
    case texts.century:
        display = getSegment(input.segment) + getQualifer(input.year) + 'Century ' + getEra(input.era);

        break;
    case texts.centuryYear:
        display = getSegment(input.segment) + input.year + '\'s ' + getEra(input.era);
        break;
    case texts.period:
        if (input.secondYear != '' && input.thirdYear != ''){
            if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && (input.secondDay == '' || input.thirdDay == '')){
                display = getMonth(input.secondMonth) + '- ' + getMonth(input.thirdMonth) + getYear(input.secondYear) + getEra(input.secondEra);
            }
            else{
                if(input.secondYear == input.thirdYear && input.secondMonth == input.thirdMonth){
                    display = getDay(input.secondDay) + '- ' + getDay(input.thirdDay) + getMonth(input.secondMonth) 
                              + getYear(input.secondYear) + getEra(input.secondEra);
                }
                else{
                    display = getSimpleDate(input.secondDay, input.secondMonth, input.secondYear, input.secondEra) + ' - ' +
                    getSimpleDate(input.thirdDay, input.thirdMonth, input.thirdYear, input.thirdEra);
                }
            }
        }
        break;
    case texts.either:
        if (input.secondYear != '' && input.thirdYear != ''){
            if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && (input.secondDay == '' || input.thirdDay == '')){
                display = 'Either ' + getMonth(input.secondMonth) + 'or ' + getMonth(input.thirdMonth) 
                          + getYear(input.secondYear) + getEra(input.secondEra);
            }
            else{
                if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && input.secondMonth == input.thirdMonth){
                    display = 'Either ' + getDay(input.secondDay) + 'or ' + getDay(input.thirdDay) + getMonth(input.secondMonth) 
                              + getYear(input.secondYear) + getEra(input.secondEra);
                }
                else{
                    display = 'Either ' + getSimpleDate(input.secondDay, input.secondMonth, input.secondYear, input.secondEra) + ' or ' +
                    getSimpleDate(input.thirdDay, input.thirdMonth, input.thirdYear, input.thirdEra);
                }
            }
        }
        break;
    default:
        break;
    }
    return display;
}

function formatWrongDays(input){
    var display = getMonth(input.month) + getYear(input.year) + 'does not contain ' + input.day + ' days!';
    return display;
}

function formatWrongSecondDays(input){
    var display = getMonth(input.secondMonth) + getYear(input.secondYear) + 'does not contain ' + input.secondDay + ' days!';
    return display;
}

function formatWrongThirdDays(input){
    var display = getMonth(input.thirdMonth) + getYear(input.thirdYear) + 'does not contain ' + input.thirdDay + ' days!';
    return display;
}

function formatCompareError(input){
    var display = 'Earliest date is later than Latest date!';
    return display;
}

function formatMissingYear(input){
    var display;
    if (input.secondYear == ''){ 
        display = 'Earliest year is missing!';
    }
    else {
        display = 'Latest year is missing!';
    }
    return display;
}