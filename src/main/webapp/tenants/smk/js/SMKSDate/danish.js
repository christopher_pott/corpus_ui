 	function dkGetQualifer(num){
        
        return ((num == '') ? '' : num + '.');
    }

    function dkGetMonth(index){
        
        var months = new Array('januar','februar','marts','april','maj','juni','juli','august','september','oktober','november','december');
        var month = '';
        
        if(index <= months.length && index > 0){
           month = months[index-1];
        }
        return ((month == '') ? '' : month + ' ');
        
    }

    function dkGetSegment(txt){
        var seg = '';
        
        switch(txt){
        case segments[0]:
            seg = 'Hele';
            break;
        case segments[1]:
            seg = '1. halvdel af';
            break;
        case segments[2]:
            seg = '2. halvdel af';
            break;
        case segments[3]:
            seg = '1. fjerdedel af';
            break;
        case segments[4]:
            seg = '2. fjerdedel af';
            break;
        case segments[5]:
            seg = '3. fjerdedel af';
            break;
        case segments[6]:
            seg = '4. fjerdedel af';
            break;
        case segments[7]:
            seg = 'Begyndelsen af';
            break;
        case segments[8]:
            seg = 'Midten af';
            break;
        case segments[9]:
            seg = 'Slutningen af';
            break;
        }

        return ((txt == texts.whole) ? '' : seg + ' ');
    }

    function dkGetEra(txt){
        
        return ((txt == texts.ad) ? '' : 'f.Kr. ');
    }

    function dkGetDay(txt){
        
        return ((txt == '') ? '' : txt + ' ');
    }

    function dkGetYear(txt){
        
        return ((txt == '') ? '' : txt + ' ');
    }

    function dkGetSimpleDate(date, month, year, era){
        
        var txt = '';
        
        if (year != ''){
            if (month != ''){
                if (date != ''){
                    txt = date + '/';
                    txt = date + '/' + month + '/';
                }
                else{
                    txt = dkGetMonth(month);
                }
            }
            txt = txt + year;

            if (era == texts.bc){
                txt = txt + ' f.Kr.';
            }
        }
        return txt;
    }

            
    function dkFormatDate(input){
        
        var display = '';
        
        switch (input.state)
        {
        case texts.select:
            break;
        case texts.date:
            display = dkGetSegment(input.segment) + dkGetQualifer(input.day) + dkGetMonth(input.month) + 
                      dkGetYear(input.year) + dkGetEra(input.era);
            break;
        case texts.before:
            display = 'Før ' + dkGetQualifer(input.day) + dkGetMonth(input.month) + 
                      dkGetYear(input.year) + dkGetEra(input.era);
            break;
        case texts.after:
            display = 'Efter ' + dkGetQualifer(input.day) + dkGetMonth(input.month) + 
                      dkGetYear(input.year) + dkGetEra(input.era);
            break;
        case texts.circa:
            display = 'Ca. ' + dkGetQualifer(input.day) + dkGetMonth(input.month) + 
                      dkGetYear(input.year) + dkGetEra(input.era);
            break;
        case texts.decade:
            display = dkGetSegment(input.segment) + input.year + '\'erne ' + dkGetEra(input.era);
            break;
        case texts.century:
            display = dkGetSegment(input.segment) + dkGetQualifer(input.year) + ' århundrede ' + dkGetEra(input.era);

            break;
        case texts.centuryYear:
            display = dkGetSegment(input.segment) + input.year + '-tallet ' + dkGetEra(input.era);
            break;
        case texts.period:
            if (input.secondYear != '' && input.thirdYear != ''){
                if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && (input.secondDay == '' || input.thirdDay == '')){
                    display = dkGetMonth(input.secondMonth) + '- ' + dkGetMonth(input.thirdMonth) + dkGetYear(input.secondYear) + dkGetEra(input.secondEra);
                }
                else{
                    if(input.secondYear == input.thirdYear && input.secondMonth == input.thirdMonth){
                        display = dkGetDay(input.secondDay) + '- ' + dkGetDay(input.thirdDay) + dkGetMonth(input.secondMonth) 
                                  + dkGetYear(input.secondYear) + dkGetEra(input.secondEra);
                    }
                    else{
                        display = dkGetSimpleDate(input.secondDay, input.secondMonth, input.secondYear, input.secondEra) + ' - ' +
                        dkGetSimpleDate(input.thirdDay, input.thirdMonth, input.thirdYear, input.thirdEra);
                    }
                }
            }
            break;
        case texts.either:
            if (input.secondYear != '' && input.thirdYear != ''){
                if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && (input.secondDay == '' || input.thirdDay == '')){
                    display = 'Enten ' + dkGetMonth(input.secondMonth) + 'eller ' + dkGetMonth(input.thirdMonth) 
                              + dkGetYear(input.secondYear) + dkGetEra(input.secondEra);
                }
                else{
                    if(input.secondYear == input.thirdYear && input.secondEra == input.thirdEra && input.secondMonth == input.thirdMonth){
                        display = 'Enten ' + dkGetDay(input.secondDay) + 'eller ' + dkGetDay(input.thirdDay) + dkGetMonth(input.secondMonth) 
                                  + dkGetYear(input.secondYear) + dkGetEra(input.secondEra);
                    }
                    else{
                        display = 'Enten ' + dkGetSimpleDate(input.secondDay, input.secondMonth, input.secondYear, input.secondEra) + ' eller ' +
                        dkGetSimpleDate(input.thirdDay, input.thirdMonth, input.thirdYear, input.thirdEra);
                    }
                }
            }
            break;
        default:
            break;
        }
        return display;
    }

    function dkFormatWrongDays(input){
        var display = dkGetMonth(input.month) + dkGetYear(input.year) + 'indeholder ikke ' + input.day + ' dage!';
        return display;
    }

    function dkFormatWrongSecondDays(input){
        var display = dkGetMonth(input.secondMonth) + dkGetYear(input.secondYear) + 'indeholder ikke ' + input.secondDay + ' dage!';
        return display;
    }

    function dkFormatWrongThirdDays(input){
        var display = dkGetMonth(input.thirdMonth) + dkGetYear(input.thirdYear) + 'indeholder ikke ' + input.thirdDay + ' dage!';
        return display;
    }

    function dkFormatCompareError(input){
        var display = 'Tidligste dato er senere end Seneste dato!';
        return display;
    }

    function dkFormatMissingYear(input){
        var display;
        if (input.secondYear == ''){ 
            display = 'Tidligste år mangler!';
        }
        else {
            display = 'Seneste år mangler!';
        }
        return display;
    }