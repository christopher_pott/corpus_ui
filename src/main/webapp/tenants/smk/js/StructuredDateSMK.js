/*
Copyright 2011 University of California, Berkeley; Museum of Moving Image

Licensed under the Educational Community License (ECL), Version 2.0. 
You may not use this file except in compliance with this License.

You may obtain a copy of the ECL 2.0 License at
https://source.collectionspace.org/collection-space/LICENSE.txt
*/

/*global jQuery, fluid, window, cspace:true*/
"use strict";

cspace = cspace || {};

(function ($, fluid) {       	
	
    // Default options for the component.
    fluid.defaults("cspace.structuredDateSMK", {
        gradeNames: ["fluid.rendererComponent", "autoInit"],
        preInitFunction: "cspace.structuredDateSMK.preInit",
        postInitFunction: "cspace.structuredDateSMK.postInitFunction",
        finalInitFunction: "cspace.structuredDateSMK.finalInitFunction",
        selectors: {
            popupContainer: ".csc-structuredDateSMK-popup-container"
        },
        styles: {
            structuredDateSMK: "cs-structuredDateSMK-input"
        },
        root: "",
        elPath: "",
        invokers: {
            showPopup: {
                funcName: "cspace.structuredDateSMK.showPopup",
                args: "{structuredDateSMK}"
            },
            hidePopup: {
                funcName: "cspace.structuredDateSMK.hidePopup",
                args: "{structuredDateSMK}"
            }
        },
        // Sub-components of this component are declared here.
        //
        // Options that will be passed to the subcomponent(s) to override
        // their defaults also go here.
        components: {
            popup: {
                type: "cspace.structuredDateSMK.popup",
                container: "{structuredDateSMK}.popupContainer",
                options: {
                    model: "{structuredDateSMK}.model",
                    applier: "{structuredDateSMK}.applier",
                    elPaths: "{structuredDateSMK}.options.elPaths",
                    root: "{structuredDateSMK}.options.root",
                    events: {
                        removeListeners: "{structuredDateSMK}.events.removeListeners"
                    }
                }
                
            }
        },
        events: {
            removeListeners: null
        },
        listeners: {
            removeListeners: {
                listener: "{structuredDateSMK}.removeApplierListeners"
            }
        }
    });
    
    cspace.structuredDateSMK.preInit = function (that) {
        that.removeApplierListeners = function () {
            that.applier.modelChanged.removeListener("elPath-" + that.id);
        };
    };

    cspace.structuredDateSMK.finalInitFunction = function (that) {
        // Dismiss the structured date popup by pressing the ESC key
        that.union.keyup(function (event) {
            if (cspace.util.keyCode(event) === $.ui.keyCode.ESCAPE) {
                that.container.focus();
                that.hidePopup();
            }
        });

        // Open the structured date popup by pressing the Return/Enter key
        // when focus is on the container field
        //
        // Makes it possible to re-open the popup after it has been
        // dismissed using the ESC key
        that.container.keyup(function (event) {
            if (cspace.util.keyCode(event) === $.ui.keyCode.ENTER) {
                that.showPopup();
            }
        });

       // Hide the popup when focus leaves the union of the
       // container field and the structured date popup container
       fluid.deadMansBlur(that.union, {
            exclusions: {union: that.union},
            handler: that.hidePopup
        });
        
        // If the value of the summary element in the model changes,
        // update the value of the container field to reflect that change.
        if (that.options.elPath) {
            var fullElPath = cspace.util.composeSegments(that.options.root, that.options.elPath);
            that.applier.modelChanged.addListener(fullElPath, function (model) {
                that.container.val(fluid.get(model, fullElPath));
            }, "elPath-" + that.id);
        }
        
        // Show the structured date popup when focus is placed
        // in the popup container
        that.container.focus(that.showPopup);
    };
    
    cspace.structuredDateSMK.hidePopup = function (that) {
        that.popupContainer.hide();
    };
    
    var positionPopup = function (popup, container) {
        /*
            if popup window overflows screen on right
            try positioning it by (my) top right (at) right bottom (of) container
            if this causes the popup to overflow to the left 
            position it by (my) center top (at) center bottom (of) container 
            with (collision) fit horizontal, flip vertical 
        */
        var offset = popup.offset();
        if ($(window).width() - offset.left < popup.width()) {
            popup.position({
                my: "right top", 
                at: "right bottom", 
                of: container,
                collision: "none flip",
                using: function (hash) {
                    if (hash.left < 0) {
                        popup.position({
                            my: "center top",
                            at: "center bottom",
                            of: container,
                            collision: "fit flip"
                        });
                    } else {
                        popup.css({
                            "top": hash.top,
                            "left": hash.left
                        });
                    }
                }
            });
        } else {
            popup.position({
                my: "left top", 
                at: "left bottom",
                of: container
            });
        }
    };

    cspace.structuredDateSMK.showPopup = function (that) {
        that.popup.refreshView();
        that.popupContainer.show();
        positionPopup(that.popup.locate("popup"), that.container);
        
        initStructuredDate(that.popupContainer);        
        
    };
    
    cspace.structuredDateSMK.postInitFunction = function (that) {
        that.container.addClass(that.options.styles.structuredDateSMK);
        // Create a container element and attach it to the DOM.
        // The structured date popup will later be inserted within this container.
        that.popupContainer =
            $("<div/>").addClass((that.options.selectors.popupContainer).substring(1));
        that.popupContainer.hide();
        that.container.after(that.popupContainer);
        // Declare the combination of the input field that triggers
        // the popup behavior, and the popup itself, as a consolidated
        // entity on which we can define behaviors, such as loss of focus (blur).
        that.union = that.container.add(that.popupContainer);
    };
    
    // Default options for the popup sub-component.
    fluid.defaults("cspace.structuredDateSMK.popup", {
        finalInitFunction: "cspace.structuredDateSMK.popup.finalInitFunction",
        preInitFunction: "cspace.structuredDateSMK.popup.preInit",
        gradeNames: ["fluid.rendererComponent", "autoInit"],
        // When merging models between component and sub-component,
        // the "preserve" policy will share the original model object,
        // rather than using an independent copy of that object for each.
        mergePolicy: {
            "rendererOptions.applier": "applier"
        },
        protoTree: {},
        getProtoTree: "cspace.structuredDateSMK.popup.getProtoTree",
        parentBundle: "{globalBundle}",
        selectorsToIgnore: ["popup"],
        selectors: {
            popup: ".csc-structuredDateSMK-popup",
            // Also you will need a separate selector for the label "Date Text" as well
            // in order to be able to assign the label value from the message bundle and make
            // it ready for initialization.
            // NOTE: dateDateText replaced by dateDisplayDate (Rick, 05 May 2011).
            close: ".csc-structuredDateSMK-close",                                                      		                        
            dateSMKDateTypeLabel:".csc-structuredDateSMK-SMK-date-type-label",            
            dateSMKYearLabel:".csc-structuredDateSMK-SMK-year-label",
            dateSMKMonthLabel:".csc-structuredDateSMK-SMK-month-label",
            dateSMKDayLabel:".csc-structuredDateSMK-SMK-day-label",            
            dateSMKEraLabel:".csc-structuredDateSMK-SMK-era-label",
            dateSMKSegmentLabel:".csc-structuredDateSMK-SMK-segment-label",
            dateSMKEarliestLabel:".csc-structuredDateSMK-SMK-earliest-date-label",
            dateSMKLatestLabel:".csc-structuredDateSMK-SMK-latest-date-label",            
            dateSMKDisplayTextLabel:".csc-structuredDateSMK-SMK-display-text-label", 
            dateSMKDisplayEngTextLabel:".csc-structuredDateSMK-SMK-display-eng-text-label", 
            dateSMKSegmentSelector:".csc-structuredDateSMK-SMK-segment-selector",            
            dateSMKTypeSelector:".csc-structuredDateSMK-SMK-date-type-selector",
            dateSMKDateDay:".csc-structuredDateSMK-SMK-date-day",
            dateSMKDateMonth:".csc-structuredDateSMK-SMK-date-month",
            dateSMKDateYear:".csc-structuredDateSMK-SMK-date-year",
            dateSMKEraSelector:".csc-structuredDateSMK-SMK-era-selector",            
            dateSMKSecondDateDay:".csc-structuredDateSMK-SMK-second-date-day",
            dateSMKSecondDateMonth:".csc-structuredDateSMK-SMK-second-date-month",
            dateSMKSecondDateYear:".csc-structuredDateSMK-SMK-second-date-year",
            dateSMKSecondEraSelector:".csc-structuredDateSMK-SMK-second-era-selector",            
            dateSMKThirdDateDay:".csc-structuredDateSMK-SMK-third-date-day",
            dateSMKThirdDateMonth:".csc-structuredDateSMK-SMK-third-date-month",
            dateSMKThirdDateYear:".csc-structuredDateSMK-SMK-third-date-year",
            dateSMKThirdEraSelector:".csc-structuredDateSMK-SMK-third-era-selector",            
            dateSMKDisplayText:".csc-structuredDateSMK-SMK-display-text",
            dateSMKDisplayEngText:".csc-structuredDateSMK-SMK-display-eng-text"
            	
        },
        strings: {},
        stringPaths: {
        	dateSMKDateTypeLabel:"structuredDateSMK-SMK-date-type-label",
        	dateSMKYearLabel:"structuredDateSMK-SMK-year-label",
        	dateSMKMonthLabel:"structuredDateSMK-SMK-month-label",
            dateSMKDayLabel:"structuredDateSMK-SMK-day-label",            
            dateSMKEraLabel:"structuredDateSMK-SMK-era-label",
            dateSMKSegmentLabel:"structuredDateSMK-SMK-segment-label",
            dateSMKEarliestLabel:"structuredDateSMK-SMK-earliest-date-label",
            dateSMKLatestLabel:"structuredDateSMK-SMK-latest-date-label",
            dateSMKDisplayTextLabel:"structuredDateSMK-SMK-display-text-label",
            dateSMKDisplayEngTextLabel:"structuredDateSMK-SMK-display-eng-text-label",
            close: "structuredDateSMK-close"            
        },
        // This is the place to specify the template for the popup
        // (e.g. StructuredDate.html). This template will be fetched
        // and appended inside the component's container automatically
        // at render time.
        resources: {
            template: cspace.resourceSpecExpander({
                fetchClass: "slowTemplate",
                url: "%webapp/html/components/StructuredDateSMK.html",
                options: {
                    dataType: "html"
                }
            })
        },
        root: "",
        elPaths: {},
        invokers: {
            resolveFullElPath: {
                funcName: "cspace.structuredDateSMK.popup.resolveFullElPath",
                args: ["{popup}.composeElPath", "{arguments}.0"]
            },
            composeElPath: {
                funcName: "cspace.structuredDateSMK.popup.composeElPath",
                args: ["{popup}.options.elPaths", "{popup}.options.root", "{arguments}.0"]
            },
            composeRootElPath: {
                funcName: "cspace.structuredDateSMK.popup.composeRootElPath",
                args: ["{popup}.options.elPaths", "{popup}.options.root"]
            },
            updateScalarValues: {
                funcName: "cspace.structuredDateSMK.popup.updateScalarValues",
                args: ["{arguments}.0", "{arguments}.2", "{popup}"]
            }
        },
        defaultFormat: "yyyy-MM-dd",
        displayScalars: false,
        events: {
            removeListeners: null
        },
        listeners: {
            removeListeners: {
                listener: "{popup}.removeApplierListeners"
            }
        }
    });
  
    
    cspace.structuredDateSMK.popup.composeRootElPath = function (elPaths, root) {
        var path = fluid.find(elPaths, function(elPath) {
            var segs = elPath.split(".");
            return segs.slice(0, segs.length - 1).join(".");
        });
        return cspace.util.composeSegments(root, path);
    };
    
    cspace.structuredDateSMK.popup.resolveFullElPath = function (composeElPath, key) {
        var clp = "${" + composeElPath(key) + "}";
    	return "${" + composeElPath(key) + "}";
    };
    
    cspace.structuredDateSMK.popup.composeElPath = function (elPaths, root, key) {
        return cspace.util.composeSegments(root, elPaths[key]);
    };
       
    cspace.structuredDateSMK.popup.getProtoTree = function (that) {
            	    	
    	var dateTypesName = new Array();
    	var dateTypesVal = new Array();
    	var segmentsName = new Array();
    	var segmentsVal = new Array();
    	var erasName = new Array();
    	var erasVal = new Array();    	
    	    	
    	$.each(datetypes, function(key, value) {            
    		dateTypesName.push(value);
    		dateTypesVal.push(key);
        });
    	
    	$.each(segments, function(key, value) {            
            segmentsName.push(value);
            segmentsVal.push(key); 
        });

        $.each(eras, function(key, value) {               
            erasName.push(value);
            erasVal.push(key);  
        });       
    	
        

    	return {        	        	        	        	
    		
    		dateSMKTypeSelector: {
    			optionnames:dateTypesName,
    			optionlist:dateTypesVal,    			
                selection: that.resolveFullElPath("dateSMKTypeSelector")
    		},
    		
    		dateSMKSegmentSelector: {
    			optionnames:segmentsName,
    			optionlist:segmentsVal,    			
                selection: that.resolveFullElPath("dateSMKSegmentSelector")
    		},
    		    		    		
    		dateSMKEraSelector: {
    			optionnames:erasName,
    			optionlist:erasVal,    			
                selection: that.resolveFullElPath("dateSMKEraSelector")
    		}, 
    		
    		dateSMKSecondEraSelector: {
    			optionnames:erasName,
    			optionlist:erasVal,    			
                selection: that.resolveFullElPath("dateSMKSecondEraSelector")
    		}, 
    		
    		dateSMKThirdEraSelector: {
    			optionnames:erasName,
    			optionlist:erasVal,    			
                selection: that.resolveFullElPath("dateSMKThirdEraSelector")
    		}, 
            
    		dateSMKDateDay: that.resolveFullElPath("dateSMKDateDay"),
            dateSMKDateMonth: that.resolveFullElPath("dateSMKDateMonth"),
            dateSMKDateYear: that.resolveFullElPath("dateSMKDateYear"),            
    		
            dateSMKSecondDateDay: that.resolveFullElPath("dateSMKSecondDateDay"),
            dateSMKSecondDateMonth: that.resolveFullElPath("dateSMKSecondDateMonth"),
            dateSMKSecondDateYear: that.resolveFullElPath("dateSMKSecondDateYear"),
                        
            dateSMKThirdDateDay: that.resolveFullElPath("dateSMKThirdDateDay"),
            dateSMKThirdDateMonth: that.resolveFullElPath("dateSMKThirdDateMonth"),
            dateSMKThirdDateYear: that.resolveFullElPath("dateSMKThirdDateYear"),
            
            dateSMKDisplayText: that.resolveFullElPath("dateSMKDisplayText"),
            
            dateSMKDisplayEngText: that.resolveFullElPath("dateSMKDisplayEngText"),
            
            dateSMKEarliestScalarValue: that.resolveFullElPath("dateSMKEarliestScalarValue"),
    		dateSMKLatestScalarValue: that.resolveFullElPath("dateSMKLatestScalarValue"),
                        
    		dateSMKDateTypeLabel: {
                messagekey: that.options.stringPaths.dateSMKDateTypeLabel
            },
    		
            dateSMKYearLabel: {
                messagekey: that.options.stringPaths.dateSMKYearLabel
            },

            dateSMKMonthLabel: {
                messagekey: that.options.stringPaths.dateSMKMonthLabel
            },
            
            dateSMKDayLabel: {
                messagekey: that.options.stringPaths.dateSMKDayLabel
            },
            
            dateSMKEraLabel: {
                messagekey: that.options.stringPaths.dateSMKEraLabel
            },
            
            dateSMKSegmentLabel: {
                messagekey: that.options.stringPaths.dateSMKSegmentLabel
            },
            
            dateSMKEarliestLabel: {
                messagekey: that.options.stringPaths.dateSMKEarliestLabel
            },
            
            dateSMKLatestLabel: {
                messagekey: that.options.stringPaths.dateSMKLatestLabel
            },
            
            dateSMKDisplayTextLabel: {
                messagekey: that.options.stringPaths.dateSMKDisplayTextLabel
            },
            
            dateSMKDisplayEngTextLabel: {
                messagekey: that.options.stringPaths.dateSMKDisplayEngTextLabel
            }                           
        };
    };
    
    cspace.structuredDateSMK.popup.finalInitFunction = function (that) {    	
        that.options.protoTree = fluid.invokeGlobalFunction(that.options.getProtoTree, [that]);
        var scalarValuesComputedPath = that.composeElPath("scalarValuesComputed");
        // if (scalarValuesComputedPath && fluid.get(that.model, scalarValuesComputedPath)) { // ---> this line was changed for the following line so that to solve the bug of indirect created records in CS, where scalarValuesComputed was null in that.model 
        if (scalarValuesComputedPath) {
            that.applier.modelChanged.addListener(that.composeRootElPath(), that.updateScalarValues, "scalar-" + that.id);
        }                  	
    };
        
    
    cspace.structuredDateSMK.popup.preInit = function (that) {
        that.removeApplierListeners = function () {
            that.applier.modelChanged.removeListener("scalar-" + that.id);
        };
        
    };

    
    // Fetching / Caching
    // ----------------------------------------------------
    
    // Call to primeCacheFromResources will start fetching/caching
    // of the template on this file load before the actual component's
    // creator function is called.
    fluid.fetchResources.primeCacheFromResources("cspace.structuredDateSMK.popup");
    
}(jQuery, fluid));
